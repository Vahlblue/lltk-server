#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
Definition of submission node schema
"""
import json
from datetime import datetime
from typing import Optional, Any

import peewee as pw
from pydantic import BaseModel, Field, Json
from pydantic.utils import GetterDict


class SubmissionNodeGetter(GetterDict):
    def get(self, key: Any, default: Any = None) -> Any:
        if key == 'node':
            return self._obj.node.id
        if key == 'submission':
            return self._obj.submission.id
        if key == 'data':
            return json.dumps(self._obj.data)
        res = getattr(self._obj, key, default)
        if isinstance(res, pw.ModelSelect):
            return list(res)
        return res


class SubmissionNode(BaseModel):
    """Submission node schema"""
    submission: int = Field(None, description='Submission Node identifier')
    node: int = Field(..., description='Node to which the data was submitted', example=0)
    data: Json = Field(..., description='The submitted data', example="{}")
    created_at: Optional[datetime] = Field(..., description='When the submission node was created',
                                           example=datetime.utcnow())
    updated_at: Optional[datetime] = Field(..., description='When the submission node was edited',
                                           example=datetime.utcnow())

    class Config:
        orm_mode = True
        getter_dict = SubmissionNodeGetter
