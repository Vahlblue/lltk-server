#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
Definition of submission schema
"""
from datetime import datetime
from typing import Any, Dict, List, Optional

import peewee as pw
from pydantic import BaseModel, Field
from pydantic.utils import GetterDict

from schemas import AllOptional


def manual_submission_getter(input_data: Dict) -> Dict:
    if 'submission_nodes' in input_data:
        ids = []
        for n in input_data['submission_nodes']:
            ids.append(n.id)
        input_data['submission_nodes'] = ids
    return input_data


class SubmissionGetter(GetterDict):
    def get(self, key: Any, default: Any = None) -> Any:
        if key == 'node':
            return self._obj.node.id
        if key == 'submitter':
            return self._obj.submitter.id
        res = getattr(self._obj, key, default)
        if isinstance(res, pw.ModelSelect):
            return list(res)
        return res


class Submission(BaseModel):
    """Submission schema"""
    id: int = Field(None, description='Campaign Node identifier')
    node: int = Field(..., description='Node to which the data was submitted', example=0)
    submission_nodes: List[int] = Field(..., description='Node to which the data was submitted', example=0)
    submitter: int = Field(..., description='User who submitted the data', example=0)
    points_earned: Optional[int] = Field(..., description='Points earned', example=30)
    is_complete: bool = Field(False, description='If the data is final', example=True)
    created_at: datetime = Field(..., description='When the submission was created', example=datetime.utcnow())
    updated_at: datetime = Field(..., description='When the submission was edited', example=datetime.utcnow())

    class Config:
        orm_mode = True
        getter_dict = SubmissionGetter


class SubmissionUpdate(Submission, metaclass=AllOptional):
    pass
