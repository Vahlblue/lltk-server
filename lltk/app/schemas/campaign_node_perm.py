#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
Definition of permissions schema
"""
from datetime import datetime
from typing import Optional, Any

import peewee as pw
from pydantic import BaseModel, Field
from pydantic.utils import GetterDict


class CampaignNodePermGetter(GetterDict):
    def get(self, key: Any, default: Any = None) -> Any:
        if key == 'node':
            return self._obj.node.id
        if key == 'user':
            return self._obj.user.id
        res = getattr(self._obj, key, default)
        if isinstance(res, pw.ModelSelect):
            return list(res)
        return res


class CampaignNodePermission(BaseModel):
    """CampaignNodePermission object"""
    node: int = Field(..., description='Node related with this permission', example=0)
    user: int = Field(..., description='User related with this permission', example=0)
    can_view: bool = Field(False, description='If the user can view the node data', example=False)
    can_submit: bool = Field(False, description='If the user can submit data to the node', example=False)
    can_see_data: bool = Field(False, description='If the user can see submitted data', example=False)
    can_edit: bool = Field(False, description='If the user can edit the data', example=False)
    can_view_inherited: bool = Field(False, description='If the permission on if the user can view the node'
                                                        ' is inherited.', example=False)
    can_submit_inherited: bool = Field(False, description='If the permission on if the user can submit data to the node'
                                                          ' is inherited.', example=False)
    can_see_data_inherited: bool = Field(False, description='If the permission on if the user can see data of the node'
                                                            ' is inherited.', example=False)
    can_edit_inherited: bool = Field(False, description='If the permission on if the user can edit the node'
                                                        ' is inherited.', example=False)
    created_at: Optional[datetime] = Field(..., description='When the permission was created',
                                           example=datetime.utcnow())
    updated_at: Optional[datetime] = Field(..., description='When the permission was edited', example=datetime.utcnow())

    class Config:
        orm_mode = True
        getter_dict = CampaignNodePermGetter


class CampaignNodePermissionIn(BaseModel):
    can_view: Optional[bool] = Field(None, description='If the user can view the campaign', example=True, null=True)
    can_submit: Optional[bool] = Field(None, description='If the user can submit data to the node',
                                       example=False, null=True)
    can_see_data: Optional[bool] = Field(None, description='If the user can see submitted data',
                                         example=False, null=True)
    can_edit: Optional[bool] = Field(None, description='If the user can edit the campaign', example=False, null=True)

    class Config:
        orm_mode = True
        getter_dict = CampaignNodePermGetter
