#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
Definition of a campaign schema
"""
import json
from datetime import datetime
from typing import Optional, Any, List, Dict

import peewee as pw
from pydantic import BaseModel, Field, Json
from pydantic.utils import GetterDict

from schemas import AllOptional


def manual_campaign_node_getter(input_data: Dict) -> Dict:
    if 'data' in input_data:
        input_data['data'] = json.dumps(input_data['data'])
    if 'children' in input_data:
        ids = []
        for n in input_data['children']:
            ids.append(n.id)
        input_data['children'] = ids
    return input_data


class CampaignNodeGetter(GetterDict):
    def get(self, key: Any, default: Any = None) -> Any:
        if key == 'data':
            return json.dumps(self._obj.data)
        if key == 'parent':
            if self._obj.parent is None:
                return None
            return self._obj.parent.id
        res = getattr(self._obj, key, default)
        if isinstance(res, pw.ModelSelect):
            return list(res)
        return res


class CampaignNode(BaseModel):
    """CampaignNode object"""
    parent: Optional[int] = Field(..., description='The parent node', example=0, null=True)
    data: Json = Field(..., description='The node data', example="{}")
    type: str = Field(..., description='The node type', example="QCM")
    points: int = Field(10, description='Points earned when answering the node', example=10)

    submittable: Optional[bool] = Field(False, description='If the node can have data submitted.', example=False)
    can_submit_more_than_once: Optional[bool] = Field(False,
                                                      description='If the node can have data submitted.', example=False)
    must_be_submitted: Optional[bool] = Field(False,
                                              description='If the node must have data submitted if applicable '
                                                          'and if all children node have been validated.',
                                              example=False)

    submit_root: bool = Field(...,
                              description='If the node can be have submission beginning here.',
                              example=False)

    everyone_can_see: Optional[bool] = Field(True,
                                             description='If the node can been seen by everyone by default.',
                                             example=True)
    everyone_can_submit: Optional[bool] = Field(True,
                                                description='If everyone can submit data by default', example=True)
    everyone_can_see_data: Optional[bool] = Field(False, description='If everyone can see data by default',
                                                  example=False)

    class Config:
        orm_mode = True
        getter_dict = CampaignNodeGetter


class CampaignNodeOut(CampaignNode):
    id: int = Field(None, description='Campaign Node identifier')
    children: List[int] = Field(..., description='The children nodes', example=[])
    created_at: datetime = Field(..., description='When the campaign node was created',
                                 example=datetime.utcnow())


class CampaignNodeUpdate(CampaignNode, metaclass=AllOptional):
    pass
