#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Main functions for the fastAPI app
"""
import os

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from filelock import FileLock

from models.migration import migrate
from tools.db import db, is_sqlite

# Lock the db on table creation
db_lock = FileLock("/tmp/db.lock")


def create_app():
    """Return the FastAPI app for the user microservice"""
    app = FastAPI(
        root_path=os.environ.get('BASE_URL', default="/"),
        title='LLTK',
        version='WIP',
        description="An API for a Living Lab platform"
    )

    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    from apis import user, auth, campaign, campaign_node, submission
    app.include_router(auth.router)
    app.include_router(user.router)
    app.include_router(campaign.router)
    app.include_router(campaign_node.router)
    app.include_router(submission.router)

    # Locking the db with a file to ensure other workers doesn't try to create the tables at the same time
    with db_lock:
        db.connect()
        if is_sqlite:
            db.pragma('foreign_keys', 1, permanent=True)
        migrate()
        db.close()

    secret_key = os.environ.get('SECRET_KEY', default="secretK")

    return app
