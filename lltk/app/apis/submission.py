#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
API of submission node management
"""
import os
from datetime import datetime
from typing import List, Dict

from fastapi import APIRouter, HTTPException, Depends
from playhouse.shortcuts import model_to_dict
from pydantic import Json
from starlette import status
from starlette.responses import Response, PlainTextResponse
import pandas as pd

from apis.campaign_node import node_not_found
from models.campaign_node import get_node_if_available
from models.submission import Submission as SubmissionDAO, get_submission_if_available, get_available_submissions, \
    get_completed_submissions
from models.submission_node import SubmissionNode as SubmissionNodeDAO, get_submission_node_if_available, \
    get_submission_node_if_available_by_node, validate
from models.submission_validation.errors import ValidationError
from models.user import User as UserDAO
from schemas.submission import Submission, manual_submission_getter
from schemas.submission_node import SubmissionNode
from tools.auth import get_current_user
from tools.db import get_db

router = APIRouter(
    prefix="/campaign",
    tags=["campaign", "submission"],
)

submission_not_found = HTTPException(
    status_code=status.HTTP_404_NOT_FOUND,
    detail="This submission doesn't exist or you don't have the rights to see it.")

submission_no_data = HTTPException(
    status_code=status.HTTP_404_NOT_FOUND,
    detail="This node doesn't have any data attached to it or you don't have the rights to see it.")

submission_node_not_found = HTTPException(
    status_code=status.HTTP_404_NOT_FOUND,
    detail="This submission node doesn't exist or you don't have the rights to see it.")

cant_submit = HTTPException(
    status_code=status.HTTP_409_CONFLICT,
    detail="You can't submit to this node.")

already_submitted = HTTPException(
    status_code=status.HTTP_409_CONFLICT,
    detail="Already submitted")

cant_delete = HTTPException(
    status_code=status.HTTP_403_FORBIDDEN,
    detail="You can't delete this submission.")

cant_update_validated = HTTPException(
    status_code=status.HTTP_403_FORBIDDEN,
    detail="You can't update this submission node. It was already validated.")


@router.get('/node/{node_id}/submission/', response_model=List[Submission],
            dependencies=[Depends(get_db)])
def get_submissions_list(node_id: int,
                         login_info: tuple[UserDAO, List[str]] = Depends(get_current_user)) -> List[Dict]:
    """
    Get submissions available to the user for a node
    """
    user, _ = login_info
    node = get_node_if_available(node_id, user)
    if node is None:
        raise node_not_found
    submissions = get_available_submissions(node, user)

    submissions_out = []
    for s in submissions:
        s_out = model_to_dict(s, recurse=False)
        s_out['submission_nodes'] = s.get_children()
        submissions_out.append(manual_submission_getter(s_out))
    return submissions_out


@router.get('/node/{node_id}/submission/data', dependencies=[Depends(get_db)],
            responses={
                200: {
                    "content":     {"text/csv": {}},
                    "description": "Return the CSV with the submissions data",
                }
            },
            )
def get_submissions_data(node_id: int,
                         login_info: tuple[UserDAO, List[str]] = Depends(get_current_user)) -> Response:
    """
    Get submissions data to the user for a node
    """
    user, _ = login_info
    node = get_node_if_available(node_id, user)
    if node is None:
        raise node_not_found
    submissions = get_completed_submissions(node, user)
    children = node.get_children(user)
    children_view = [
        n for n in children if n.can_see_data(user)
    ]

    print(submissions)

    if len(children_view) == 0:
        raise submission_no_data

    header = [("user_id", "user_id", "")] \
             + [(n.id,
                 n.data["name"] if "name" in n.data else "",
                 n.type) for n in children_view]
    data = {s.id: s.flatten(children_view) for s in submissions}

    print(data)
    cols = ["user_id"] + [c.id for c in children_view]

    df = pd.DataFrame.from_dict(data, columns=cols, orient='index')
    df.columns = pd.MultiIndex.from_tuples(header)

    return Response(content=df.to_csv(), media_type="text/csv")


@router.post('/node/{node_id}/submission/', response_model=Submission, status_code=status.HTTP_201_CREATED,
             dependencies=[Depends(get_db)])
def create_submission(node_id: int, login_info: tuple[UserDAO, List[str]] = Depends(get_current_user)) \
        -> Dict:
    """
    Create a new submission
    The submission must be created for a node :
    1) which the user can view
    2) which have submit_root==true
    """
    user, _ = login_info

    node = get_node_if_available(node_id, user)
    if node is None:
        raise node_not_found

    if not node.submit_root:
        raise cant_submit

    if not node.has_submit_permission(user):
        raise cant_submit

    if not node.can_submit_more_than_once:
        if SubmissionDAO.select().where((SubmissionDAO.submitter == user) & (SubmissionDAO.node == node)).count() > 0:
            raise already_submitted

    submission = SubmissionDAO(node=node,
                               submitter=user)
    submission.save(force_insert=True)
    submission_out = model_to_dict(submission, recurse=False)
    submission_out['submission_nodes'] = submission.get_children()
    return manual_submission_getter(submission_out)


@router.get('/node/submission/{submission_id}', response_model=Submission, dependencies=[Depends(get_db)])
def get_submission(submission_id: int, login_info: tuple[UserDAO, List[str]] = Depends(get_current_user)) -> Dict:
    """
    Get a node given its identifier
    """
    user, _ = login_info
    submission = get_submission_if_available(submission_id, user)
    if submission is None:
        raise submission_not_found
    submission_out = model_to_dict(submission, recurse=False)
    submission_out['submission_nodes'] = submission.get_children()
    return manual_submission_getter(submission_out)


@router.delete('/node/submission/{submission_id}', status_code=status.HTTP_204_NO_CONTENT,
               dependencies=[Depends(get_db)])
def delete_submission(submission_id: int, login_info: tuple[UserDAO, List[str]] = Depends(get_current_user)) \
        -> Response:
    """
    Delete a submission given its identifier.
    """
    user, _ = login_info
    submission = get_submission_if_available(submission_id, user)
    if submission is None:
        raise node_not_found

    if submission.submitter != user and not submission.node.can_edit(user):
        raise cant_delete

    submission.delete_instance()
    return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.post('/node/submission/{submission_id}/submit', response_model=Submission, dependencies=[Depends(get_db)])
def submit_submission(submission_id: int, login_info: tuple[UserDAO, List[str]] = Depends(get_current_user)) \
        -> PlainTextResponse:
    """
    Validate the submission.
    After that the submission will be available to the user having see_data permission and will become read only.
    """
    user, _ = login_info
    submission = get_submission_if_available(submission_id, user)
    if submission is None:
        raise submission_not_found
    if submission.is_complete:
        raise cant_update_validated
    try:
        submission.validate()
    except ValidationError as err:
        return PlainTextResponse(err.message, status_code=status.HTTP_409_CONFLICT)

    return PlainTextResponse("The submission was successfully submitted", status_code=status.HTTP_200_OK)


"""
Submission node
"""


@router.put('/node/submission/{submission_id}/node/{node_id}', response_model=SubmissionNode,
            status_code=status.HTTP_201_CREATED, dependencies=[Depends(get_db)])
def update_submission_node(submission_id: int, node_id: int, data: Json,
                           login_info: tuple[UserDAO, List[str]] = Depends(get_current_user)) -> SubmissionNode:
    """
    Create or update a new submission node
    """
    user, _ = login_info

    submission = get_submission_if_available(submission_id, user)
    if submission is None:
        raise node_not_found

    if submission.is_complete:
        raise cant_update_validated

    node = get_node_if_available(node_id, user)
    if node is None:
        raise node_not_found
    if not node.can_submit(user):
        raise cant_submit

    validate(node, data)

    submission_node = SubmissionNodeDAO.get_or_none(submission=submission, node=node)
    if submission_node is None:
        submission_node = SubmissionNodeDAO(submission=submission,
                                            node=node,
                                            data=data)
        submission_node.save(force_insert=True)
    else:
        # SubmissionNodeDAO.update(data=data).where()
        submission_node.update(data=data, updated_at=datetime.utcnow())
    return submission_node


@router.get('/node/submission/node/{submission_node_id}', response_model=SubmissionNode,
            dependencies=[Depends(get_db)])
def get_submission_node(submission_node_id: int,
                        login_info: tuple[UserDAO, List[str]] = Depends(get_current_user)) -> Dict:
    """
    Get a submission node given its identifier
    """
    user, _ = login_info
    submission_node = get_submission_node_if_available(submission_node_id, user)
    if submission_node is None:
        raise submission_not_found
    return submission_node


@router.get('/node/submission/{submission_id}/node/{node_id}', response_model=SubmissionNode,
            dependencies=[Depends(get_db)])
def get_submission_node_bis(submission_id: int, node_id: int,
                            login_info: tuple[UserDAO, List[str]] = Depends(get_current_user)) -> Dict:
    """
    Get a node given its identifier
    """
    user, _ = login_info
    submission_node = get_submission_node_if_available_by_node(submission_id, node_id, user)
    if submission_node is None:
        raise submission_node_not_found
    return submission_node


@router.delete('/node/submission/node/{submission_node_id}', status_code=status.HTTP_204_NO_CONTENT,
               dependencies=[Depends(get_db)])
def delete_submission_node(submission_node_id: int, login_info: tuple[UserDAO, List[str]] = Depends(get_current_user)) \
        -> Response:
    """
    Delete a submission given its identifier.
    """
    user, _ = login_info
    submission_node = get_submission_node_if_available(submission_node_id, user)
    if submission_node is None:
        raise submission_node_not_found

    if submission_node.submission.is_complete:
        raise cant_update_validated

    submission_node.delete_instance()
    return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.delete('/node/submission/{submission_id}/node/{node_id}/', status_code=status.HTTP_204_NO_CONTENT,
               dependencies=[Depends(get_db)])
def delete_submission_node_bis(node_id: int, submission_id: int,
                               login_info: tuple[UserDAO, List[str]] = Depends(get_current_user)) \
        -> Response:
    """
    Delete a submission given its identifier.
    """
    user, _ = login_info
    submission_node = get_submission_node_if_available_by_node(submission_id, node_id, user)
    if submission_node is None:
        raise submission_node_not_found
    if submission_node.submission.is_complete:
        raise cant_update_validated
    submission_node.delete_instance()
    return Response(status_code=status.HTTP_204_NO_CONTENT)
