#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
API of campaign node management
"""

from datetime import datetime
from typing import List, Dict

from fastapi import APIRouter, HTTPException, Depends
from playhouse.shortcuts import model_to_dict
from starlette import status
from starlette.responses import Response

from apis.user import user_not_found
from models.campaign_node import CampaignNode as CampaignNodeDAO, get_node_if_editable, get_node_if_available
from models.campaign_node_perm import CampaignNodePermission as CampaignNodePermissionDAO
from models.user import User as UserDAO
from schemas.campaign_node import CampaignNode, CampaignNodeUpdate, CampaignNodeOut, manual_campaign_node_getter
from schemas.campaign_node_perm import CampaignNodePermission, CampaignNodePermissionIn
from tools.auth import get_current_user
from tools.db import get_db

router = APIRouter(
    prefix="/campaign/node",
    tags=["campaign", "node"],
)

node_not_found = HTTPException(
    status_code=status.HTTP_404_NOT_FOUND,
    detail="This node doesn't exist or you don't have the rights to see it.")

parent_node_not_found = HTTPException(
    status_code=status.HTTP_404_NOT_FOUND,
    detail="The parent node doesn't exist or you don't have the rights to see it.")

no_root_creation = HTTPException(
    status_code=status.HTTP_403_FORBIDDEN,
    detail="You can't create a root node")

no_root_removal = HTTPException(
    status_code=status.HTTP_403_FORBIDDEN,
    detail="You can't remove a root node")

no_recursion = HTTPException(
    status_code=status.HTTP_412_PRECONDITION_FAILED,
    detail="You can't change parent node to itself or a children.")


@router.post('/', response_model=CampaignNodeOut, status_code=status.HTTP_201_CREATED, dependencies=[Depends(get_db)])
def create_node(body: CampaignNode, login_info: tuple[UserDAO, List[str]] = Depends(get_current_user)) \
        -> CampaignNodeOut:
    """
    Create a new node
    You can't create a root node
    """
    user, _ = login_info
    payload = body.dict()

    if 'parent' not in payload or payload['parent'] is None:
        raise no_root_creation

    if payload['type'] == "root":
        raise no_root_creation

    node = get_node_if_editable(payload['parent'], user)
    if node is None:
        raise node_not_found

    node = CampaignNodeDAO(**payload)
    node.save(force_insert=True)
    return node


@router.get('/{node_id}', response_model=CampaignNodeOut, dependencies=[Depends(get_db)])
def get_node(node_id: int, login_info: tuple[UserDAO, List[str]] = Depends(get_current_user)) -> Dict:
    """
    Get a node given its identifier
    """
    user, _ = login_info
    node = get_node_if_available(node_id, user)
    if node is None:
        raise node_not_found
    node_out = model_to_dict(node, recurse=False)
    node_out['children'] = node.get_children(user)
    return manual_campaign_node_getter(node_out)


@router.put('/{node_id}', response_model=CampaignNodeOut, dependencies=[Depends(get_db)])
def update_node(node_id: int, body: CampaignNodeUpdate,
                login_info: tuple[UserDAO, List[str]] = Depends(get_current_user)) \
        -> Dict:
    """
    Update a node given its identifier
    """
    user, _ = login_info
    payload = body.dict(exclude_unset=True)

    node = get_node_if_editable(node_id, user)
    if node is None:
        raise node_not_found

    if 'parent' in payload:
        if 'parent' is None:
            raise no_root_creation
        parent = get_node_if_editable(payload['parent'], user)
        if parent is None:
            raise parent_node_not_found
        if node.parent is None:
            raise no_root_removal
        if parent == node:
            raise no_recursion
        if parent in node.get_children(user):
            raise no_recursion
        payload['parent'] = parent

    if node.parent is None:
        for x in ('submittable', 'must_be_submitted', 'type'):
            if x in payload:
                raise HTTPException(
                    status_code=status.HTTP_403_FORBIDDEN,
                    detail=f"You can't update {x} for a root node")

    CampaignNodeDAO.update(payload).where(CampaignNodeDAO.id == node_id).execute()
    node_out = model_to_dict(node, recurse=False)
    node_out['children'] = node.get_children(user)
    return manual_campaign_node_getter(node_out)


@router.delete('/{node_id}', status_code=status.HTTP_204_NO_CONTENT, dependencies=[Depends(get_db)])
def delete_node(node_id: int, login_info: tuple[UserDAO, List[str]] = Depends(get_current_user)) -> Response:
    """
    Delete a node given its identifier.
    If the node is a root, the children are deleted.
    """
    user, _ = login_info
    node = get_node_if_editable(node_id, user)
    if node is None:
        raise node_not_found
    if node.parent is None:
        CampaignNodeDAO.delete().where(CampaignNodeDAO.parent == node_id)

    node.delete_instance()
    return Response(status_code=status.HTTP_204_NO_CONTENT)


"""
Nodes Permissions
"""


@router.get('/{node_id}/perms', response_model=List[CampaignNodePermission], dependencies=[Depends(get_db)])
def list_node_perm(node_id: int, login_info: tuple[UserDAO, List[str]] = Depends(get_current_user)) \
        -> List[CampaignNodePermission]:
    """
    List all perms of a node
    """
    user, _ = login_info
    node = get_node_if_editable(node_id, user)
    if node is None:
        raise node_not_found
    return list(CampaignNodePermissionDAO.select()
                .where(CampaignNodePermissionDAO.node == node))


@router.get('/{node_id}/perms/{user_id}', response_model=CampaignNodePermission, dependencies=[Depends(get_db)])
def get_node_perm(node_id: int, user_id: int, login_info: tuple[UserDAO, List[str]] = Depends(get_current_user)) \
        -> CampaignNodePermission:
    """
    Get a node perm given its node and user
    """
    user, _ = login_info
    if user.id == user_id:
        node = get_node_if_available(node_id, user)
    else:
        node = get_node_if_editable(node_id, user)
    if node is None:
        raise node_not_found
    try:
        user2 = UserDAO.get(UserDAO.id == user_id)
    except UserDAO.DoesNotExist:
        raise user_not_found
    perm = CampaignNodePermissionDAO.get_or_none((CampaignNodePermissionDAO.node == node_id)
                                                 & (CampaignNodePermissionDAO.user == user_id))
    if perm is None:
        return CampaignNodePermission(
            node=node_id,
            user=user_id,
            can_view=node.can_view(user2),
            can_submit=node.can_see_data(user2),
            can_see_data=node.can_view(user2),
            can_edit=node.can_edit(user2),
            can_view_inherited=True,
            can_submit_inherited=True,
            can_see_data_inherited=True,
            can_edit_inherited=True,
            created_at=datetime.utcnow(),
            updated_at=datetime.utcnow())

    perm_out = model_to_dict(perm, recurse=False)
    if perm_out['can_view'] is None:
        perm_out['can_view'] = node.can_view(user2)
        perm_out['can_view_inherited'] = True
    if perm_out['can_submit'] is None:
        perm_out['can_submit'] = node.can_submit(user2)
        perm_out['can_submit_inherited'] = True
    if perm_out['can_see_data'] is None:
        perm_out['can_see_data'] = node.can_see_data(user2)
        perm_out['can_see_data_inherited'] = True
    if perm_out['can_edit'] is None:
        perm_out['can_edit'] = node.can_edit(user2)
        perm_out['can_edit_inherited'] = True
    return CampaignNodePermission(**perm_out)


@router.put('/{node_id}/perms/{user_id}', response_model=CampaignNodePermission, dependencies=[Depends(get_db)])
def update_node_perm(node_id: int, user_id: int, body: CampaignNodePermissionIn,
                     login_info: tuple[UserDAO, List[str]] = Depends(get_current_user)) \
        -> CampaignNodePermission:
    """
    Update a node perm given the node and user
    """
    user, _ = login_info
    payload = body.dict(exclude_unset=True)

    node = get_node_if_editable(node_id, user)
    if node is None:
        raise node_not_found
    try:
        user2 = UserDAO.get(UserDAO.id == user_id)
    except UserDAO.DoesNotExist:
        raise user_not_found
    perm = CampaignNodePermissionDAO.get_or_none((CampaignNodePermissionDAO.node == node_id)
                                                 & (CampaignNodePermissionDAO.user == user_id))
    if perm is None:
        payload['node'] = node_id
        payload['user'] = user_id
        perm = CampaignNodePermissionDAO(**payload)
        perm.save(force_insert=True)
        perm_out = model_to_dict(perm, recurse=False)
    else:
        payload['updated_at'] = datetime.utcnow()
        CampaignNodePermissionDAO.update(payload).where((CampaignNodePermissionDAO.node == node_id)
                                                        & (CampaignNodePermissionDAO.user == user_id)).execute()
        perm_out = model_to_dict(perm, recurse=False)
        perm_out.update(**payload)
        if ('can_view' not in payload or payload['can_view'] is None) \
                and ('can_submit' not in payload or payload['can_submit'] is None) \
                and ('can_see_data' not in payload or payload['can_see_data'] is None) \
                and ('can_edit' not in payload or payload['can_edit'] is None):
            perm.delete_instance()

    if perm_out['can_view'] is None:
        perm_out['can_view'] = node.can_view(user2)
        perm_out['can_view_inherited'] = True
    if perm_out['can_submit'] is None:
        perm_out['can_submit'] = node.can_submit(user2)
        perm_out['can_submit_inherited'] = True
    if perm_out['can_see_data'] is None:
        perm_out['can_see_data'] = node.can_see_data(user2)
        perm_out['can_see_data_inherited'] = True
    if perm_out['can_edit'] is None:
        perm_out['can_edit'] = node.can_edit(user2)
        perm_out['can_edit_inherited'] = True
    return CampaignNodePermission(**perm_out)


@router.delete('/{node_id}/perms/{user_id}', status_code=status.HTTP_204_NO_CONTENT, dependencies=[Depends(get_db)])
def delete_node_perm(node_id: int, user_id: int,
                     login_info: tuple[UserDAO, List[str]] = Depends(get_current_user)) -> Response:
    """
    Delete a node perm given the node and user. Put the perm in inherited state
    """
    user, _ = login_info
    node = get_node_if_editable(node_id, user)
    if node is None:
        raise node_not_found
    try:
        user2 = UserDAO.get(UserDAO.id == user_id)
    except UserDAO.DoesNotExist:
        raise user_not_found
    perm = CampaignNodePermissionDAO.get_or_none((CampaignNodePermissionDAO.node == node)
                                                 & (CampaignNodePermissionDAO.user == user2))

    if perm is not None:
        perm.delete_instance()

    return Response(status_code=status.HTTP_204_NO_CONTENT)
