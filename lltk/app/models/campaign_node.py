#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Definition of a campaign node and tools associated

view permission must have node view permission and campaign view permission
edit permission must have campaign view permission
submit permission must have view permission
see_data permission must have view permission

admin => edit permission => view permission
admin => submit + see_data permissions

Campaign permissions :
view permission ; is_public (Campaign) < can_view (CampaignPermission) < is_admin (Campaign)
other permission : can_X (CampaignPermission) < is_admin (Campaign)

Node permissions :
everyone_can_X (CampaignNode) < can_X (CampaignNodePermission) < is_admin (Campaign)
Permission lower in the tree are stronger.
"""

from __future__ import annotations

from datetime import datetime
from typing import TYPE_CHECKING, Optional, List

import peewee as pw
from playhouse.postgres_ext import BinaryJSONField

from tools.db import db

if TYPE_CHECKING:
    # Avoid circular import
    from models.user import User
    from models.campaign import Campaign


class CampaignNode(pw.Model):
    """CampaignNode object"""
    id = pw.AutoField()
    parent = pw.ForeignKeyField('self', backref="children", null=True, on_delete='CASCADE', on_update='CASCADE')
    type = pw.CharField()
    points = pw.IntegerField(default=10, constraints=[pw.Check('points >= 0')])
    data = BinaryJSONField()

    submittable = pw.BooleanField(default=False)
    can_submit_more_than_once = pw.BooleanField(default=False)
    submit_root = pw.BooleanField(default=False)
    must_be_submitted = pw.BooleanField(default=True)

    everyone_can_see = pw.BooleanField(default=True)
    everyone_can_submit = pw.BooleanField(default=True)
    everyone_can_see_data = pw.BooleanField(default=False)

    created_at = pw.DateTimeField(default=datetime.utcnow)

    class Meta:
        database = db
        table_name = "campaign_node"

    def get_campaign(self) -> Campaign:
        """
        Return the campaign related with this node
        """
        if self.parent is None:
            return self.campaign.get()
        return self.parent.get_campaign()

    def p_has_not_no_strong_can_view(self, user: User) -> bool:
        """
        Should not be used outside the class
        Return true if the user have a strong perm to see the node to counter a everyone_can_see=True
        ie if a can_view (CampaignNodePermission) upstream says the contrary of everyone_can_see (or if admin)
        """
        from models.campaign_node_perm import CampaignNodePermission

        perm = CampaignNodePermission.get_or_none(user=user, node=self)
        if perm is not None and perm.can_view is not None:
            if perm.can_view:
                return True
            else:
                return False
        if self.parent is None:
            return True
        return self.parent.p_has_not_no_strong_can_view(user)

    def p_has_strong_can_view(self, user: User) -> bool:
        """
        Should not be used outside the class
        Return true if the user have a strong perm to see the node to counter a everyone_can_see=False
        ie if a can_view (CampaignNodePermission) upstream says the contrary of everyone_can_see (or if admin)
        """
        from models.campaign_node_perm import CampaignNodePermission

        perm = CampaignNodePermission.get_or_none(user=user, node=self)
        if perm is not None and perm.can_view is not None:
            if perm.can_view:
                return True
            else:
                return False
        if self.parent is None:
            return False
        else:
            return self.parent.p_has_strong_can_view(user)

    def can_view(self, user: User) -> bool:
        """
        Return true if the user has the right to see this node
        The user must : have th right for the node and the campaign
        The order of power is :
        Campaign : is_public (Campaign) < can_view (CampaignPermission) < is_admin (Campaign)
        Node : everyone_can_see (CampaignNode) < can_view (CampaignNodePermission) < is_admin (Campaign)
        Permission lower in the tree are stronger.
        """
        campaign = self.get_campaign()
        if not campaign.can_view(user):
            return False
        if campaign.is_admin(user):
            return True
        if self.everyone_can_see:
            return self.p_has_not_no_strong_can_view(user) or self.can_edit(user)
        else:
            return self.p_has_strong_can_view(user) or self.can_edit(user)

    def p_has_not_no_strong_can_submit(self, user: User) -> bool:
        """
        Should not be used outside the class
        Return true if the user have a strong perm to submit to the node to counter a everyone_can_submit=True
        ie if a can_submit (CampaignNodePermission) upstream says the contrary of everyone_can_submit (or if admin)
        """
        from models.campaign_node_perm import CampaignNodePermission

        perm = CampaignNodePermission.get_or_none(user=user, node=self)
        if perm is not None and perm.can_submit is not None:
            if perm.can_submit:
                return True
            else:
                return self.get_campaign().is_admin(user)
        if self.parent is None:
            return True
        return self.parent.p_has_not_no_strong_can_submit(user)

    def p_has_strong_can_submit(self, user: User) -> bool:
        """
        Should not be used outside the class
        Return true if the user have a strong perm to submit to the node to counter a everyone_can_submit=False
        ie if a can_submit (CampaignNodePermission) upstream says the contrary of everyone_can_submit (or if admin)
        """
        from models.campaign_node_perm import CampaignNodePermission

        perm = CampaignNodePermission.get_or_none(user=user, node=self)
        if perm is not None and perm.can_submit is not None:
            if perm.can_submit:
                return True
            else:
                return self.get_campaign().is_admin(user)
        if self.parent is None:
            return False
        else:
            return self.parent.p_has_strong_can_submit(user)

    def has_submit_permission(self, user: User) -> bool:
        """
        Return true if the user has the permission to submit to this node
        """
        if not self.can_view(user):
            return False
        if self.everyone_can_submit:
            return self.p_has_not_no_strong_can_submit(user)
        else:
            return self.p_has_strong_can_submit(user)

    def can_submit(self, user: User) -> bool:
        """
        Return true if the user has the right to submit data to this node and if this node allows submitted data
        The order of power is :
        Node : everyone_can_submit (CampaignNode) < can_submit (CampaignNodePermission) < is_admin (Campaign)
        Permission lower in the tree are stronger.
        """
        # TODO : can_submit_more_than_once
        return self.submittable and self.has_submit_permission(user)

    def p_has_not_no_strong_can_see_data(self, user: User) -> bool:
        """
        Should not be used outside the class
        Return true if the user have a strong perm to see data of the node to counter a everyone_can_see_data=True
        ie if a can_see_data (CampaignNodePermission) upstream says the contrary of everyone_can_see_data (or if admin)
        """
        from models.campaign_node_perm import CampaignNodePermission

        perm = CampaignNodePermission.get_or_none(user=user, node=self)
        if perm is not None and perm.can_see_data is not None:
            if perm.can_see_data:
                return True
            else:
                return self.get_campaign().is_admin(user)
        if self.parent is None:
            return True
        return self.parent.p_has_not_no_strong_can_see_data(user)

    def p_has_strong_can_see_data(self, user: User) -> bool:
        """
        Should not be used outside the class
        Return true if the user have a strong perm to see data of the node to counter a everyone_can_see_data=False
        ie if a can_see_data (CampaignNodePermission) upstream says the contrary of everyone_can_see_data (or if admin)
        """
        from models.campaign_node_perm import CampaignNodePermission

        perm = CampaignNodePermission.get_or_none(user=user, node=self)
        if perm is not None and perm.can_see_data is not None:
            if perm.can_see_data:
                return True
            else:
                return self.get_campaign().is_admin(user)
        if self.parent is None:
            return False
        else:
            return self.parent.p_has_strong_can_see_data(user)

    def _has_see_data_permission(self, user: User) -> bool:
        """
        Return true if the user has the permission to see data of this node
        """
        if not self.can_view(user):
            return False
        if self.everyone_can_see_data:
            return self.p_has_not_no_strong_can_see_data(user)
        else:
            return self.p_has_strong_can_see_data(user)

    def can_see_data(self, user: User) -> bool:
        """
        Return true if the user has the right to see data of this node
        The order of power is :
        Node : everyone_can_see_data (CampaignNode) < can_see_data (CampaignNodePermission) < is_admin (Campaign)
        Permission lower in the tree are stronger.
        """
        return True #self._has_see_data_permission(user)

    def can_edit(self, user: User) -> bool:
        """
        Return true if the user has the right to see data of this node and if this node allows submitted data
        The order of power is :
        Node : everyone_can_see_data (CampaignNode) < can_see_data (CampaignNodePermission) < is_admin (Campaign)
        Permission lower in the tree are stronger.
        """
        from models.campaign_node_perm import CampaignNodePermission

        if not self.get_campaign().can_view(user):
            return False

        perm = CampaignNodePermission.get_or_none(user=user, node=self)
        if perm is not None and perm.can_edit is not None:
            if perm.can_edit:
                return True
            else:
                return self.get_campaign().is_admin(user)
        if self.parent is None:
            return self.get_campaign().is_admin(user)
        else:
            return self.parent.can_edit(user)

    def get_children(self, user: User) -> List[CampaignNode]:
        """Get children nodes that are accessible by the user"""
        nodes = []
        for n in self.children:
            if n.can_view(user):
                nodes.append(n)
        return nodes

    def get_rec_children_to_validate(self) -> List[CampaignNode]:
        """Get children nodes that must have data to validate"""
        nodes = []
        if not self.must_be_submitted:
            return nodes
        if self.submittable:
            nodes.append(self)
        for n in self.children:
            nodes += n.get_rec_children_to_validate()
        return nodes


def create_root() -> CampaignNode:
    root = CampaignNode(
        parent=None,
        type="root",
        data={},
        submittable=False,
        must_be_submitted=False
    )
    root.save(force_insert=True)
    return root


def get_node_if_available(node_id: int, user: User) -> Optional[CampaignNode]:
    """Return the node if it exists and if the user can see it
    """
    try:
        node = CampaignNode[node_id]
    except CampaignNode.DoesNotExist:
        return None
    if node.can_view(user):
        return node
    return None


def get_node_if_editable(node_id: int, user: User) -> Optional[Campaign]:
    """Return the node if it exists and if the user can edit it
    """
    try:
        node = CampaignNode[node_id]
    except CampaignNode.DoesNotExist:
        return None
    if node.can_edit(user):
        return node
    return None
