#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Rename tables to have their names with underscores
"""
from playhouse.migrate import migrate

from tools.db import db, dbMigrator


def migrate_0000():
    """Rename tables to have their names with underscores"""
    with db.atomic():
        migrate(
            dbMigrator.rename_table('campaignnode', 'campaign_node'),
            dbMigrator.rename_table('campaignnodepermission', 'campaign_node_permission'),
            dbMigrator.rename_table('campaignpermission', 'campaign_permission'),
        )
