#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Migration of database
"""

from playhouse.reflection import generate_models

from tools.db import db


def migrate():
    """Migrate to the latest version"""
    from models.migration.MigrationHistory import MigrationHistory
    from models.migration.underscores_in_tables_name_0000 import migrate_0000
    from models.migration.submission_nodes_0001 import migrate_0001
    from models.migration.make_unique_root_0002 import migrate_0002
    from models.migration.add_points_0003 import migrate_0003

    __all_migrations__ = [("0000_underscores_in_tables_name", migrate_0000),
                          ("0001_submission_nodes", migrate_0001),
                          ("0002_make_campaign_root_unique", migrate_0002),
                          ("0003_add_points", migrate_0003)]

    models = generate_models(db)
    keys = models.keys()

    if 'user' not in keys:
        # We suppose the database is empty
        from models import create_tables
        create_tables()
        for name, _ in __all_migrations__:
            MigrationHistory.create(name=name)
        return
    if 'migration_history' not in keys:
        db.create_tables([MigrationHistory])
    for name, migrate_fn in __all_migrations__:
        if MigrationHistory.get_or_none(name=name) is not None:
            continue
        migrate_fn()
        MigrationHistory.create(name=name)
