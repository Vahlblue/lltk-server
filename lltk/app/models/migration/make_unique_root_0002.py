#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Make unique campaign root
"""

from playhouse.migrate import migrate
from tools.db import db, dbMigrator


def migrate_0002():
    """Make unique campaign root
    """
    with db.atomic():
        migrate(
            dbMigrator.add_not_null('campaign', 'root_id'),
            dbMigrator.add_unique('campaign', 'root_id')
        )
