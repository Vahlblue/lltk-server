#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Add points to nodes
"""

import peewee as pw
from playhouse.migrate import migrate
from tools.db import db, dbMigrator


def migrate_0003():
    """Add points to nodes
    """
    from models.submission import Submission
    points = pw.IntegerField(default=10, constraints=[pw.Check('points >= 0')])
    points_earned = pw.IntegerField(default=None, null=True, constraints=[pw.Check('points_earned >= 0')])
    points_user = pw.IntegerField(default=0, constraints=[pw.Check('points >= 0')])
    with db.atomic():
        migrate(
            dbMigrator.add_column('campaign_node', 'points', points),
            dbMigrator.add_column('submission', 'points_earned', points_earned),
            dbMigrator.add_column('user', 'points', points_user),
        )
        Submission.update(points_earned=0).where(Submission.is_complete == True).execute()
