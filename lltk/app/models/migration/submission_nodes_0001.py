#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Migration of submission nodes creation and associated
"""
from datetime import datetime

import peewee as pw
from playhouse.migrate import migrate
from playhouse.postgres_ext import BinaryJSONField

from models.campaign_node import CampaignNode
from models.submission import Submission
from tools.db import db, dbMigrator


class SubmissionNode(pw.Model):
    """Submission object"""
    id = pw.AutoField()
    submission = pw.ForeignKeyField(Submission, backref="submissions_nodes", on_delete='CASCADE', on_update='CASCADE')
    node = pw.ForeignKeyField(CampaignNode, backref="submissions_nodes", on_delete='CASCADE', on_update='CASCADE')
    data = BinaryJSONField()
    created_at = pw.DateTimeField(default=datetime.utcnow)
    updated_at = pw.DateTimeField(default=datetime.utcnow)

    class Meta:
        database = db
        table_name = "submission_node"
        indexes = (
            (('submission', 'node'), True),
        )


def migrate_0001():
    """Create submission nodes"""
    with db.atomic():
        migrate(
            dbMigrator.add_column('campaign_node', 'submit_root', pw.BooleanField(default=False)),
            dbMigrator.drop_column('submission', 'data'),
        )

    db.create_tables([SubmissionNode])
