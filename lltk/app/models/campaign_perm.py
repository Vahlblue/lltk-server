#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
Definition of permissions of a campaign model and tools associated
"""
import datetime

import peewee as pw

from models.campaign import Campaign
from models.user import User
from tools.db import db


class CampaignPermission(pw.Model):
    """CampaignPermission object"""
    campaign = pw.ForeignKeyField(Campaign, backref="campaign_permissions", on_delete='CASCADE', on_update='CASCADE')
    user = pw.ForeignKeyField(User, backref="campaign_permissions", on_delete='CASCADE', on_update='CASCADE')
    can_view = pw.BooleanField(null=True)
    is_admin = pw.BooleanField(null=True)
    created_at = pw.DateTimeField(default=datetime.datetime.utcnow)
    updated_at = pw.DateTimeField(default=datetime.datetime.utcnow)

    class Meta:
        database = db
        table_name = "campaign_permission"
        primary_key = pw.CompositeKey('campaign', 'user')
