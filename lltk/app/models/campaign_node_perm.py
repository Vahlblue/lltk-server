#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
Definition of permissions of a campaign model and tools associated
"""
import datetime

import peewee as pw

from models.campaign_node import CampaignNode
from models.user import User
from tools.db import db


class CampaignNodePermission(pw.Model):
    """CampaignNodePermission object"""
    node = pw.ForeignKeyField(CampaignNode, backref="permissions", on_delete='CASCADE', on_update='CASCADE')
    user = pw.ForeignKeyField(User, backref="permissions", on_delete='CASCADE', on_update='CASCADE')
    can_view = pw.BooleanField(null=True, default=None)
    can_submit = pw.BooleanField(null=True, default=None)
    can_see_data = pw.BooleanField(null=True, default=None)
    can_edit = pw.BooleanField(null=True, default=None)
    created_at = pw.DateTimeField(default=datetime.datetime.utcnow)
    updated_at = pw.DateTimeField(default=datetime.datetime.utcnow)

    class Meta:
        database = db
        table_name = "campaign_node_permission"
        primary_key = pw.CompositeKey('node', 'user')
