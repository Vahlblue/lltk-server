#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Errors definition when validating
"""
from typing import List

from models.campaign_node import CampaignNode


class ValidationError(Exception):
    """Base class for validation errors
    Attributes:
        message -- explanation of the error
        nodes -- nodes concerned by the exception
    """
    def __init__(self, *args: object):
        super().__init__(args)
        self.message = "Validation error"


class NoData(ValidationError):
    """Exception raised for

    Attributes:
        message -- explanation of the error
        nodes -- nodes concerned by the exception
    """
    def __init__(self, nodes: List[CampaignNode]):
        self.nodes = nodes
        ids = [str(n.id) for n in nodes]
        self.message = f"These nodes must be validated but don't have data : {', '.join(ids)}"
