#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
Definition of submission model of a campaign and tools associated
"""
from __future__ import annotations

import datetime
from typing import Optional, List, TYPE_CHECKING

import peewee as pw

from models.campaign_node import CampaignNode
from models.submission_validation.errors import NoData
from models.user import User
from tools.db import db


if TYPE_CHECKING:
    # Avoid circular import
    from models.submission_node import SubmissionNode


class Submission(pw.Model):
    """Submission object"""
    id = pw.AutoField()
    node = pw.ForeignKeyField(CampaignNode, backref="submissions", on_delete='CASCADE', on_update='CASCADE')
    submitter = pw.ForeignKeyField(User, backref="submissions", on_delete='CASCADE', on_update='CASCADE')
    is_complete = pw.BooleanField(default=False)
    points_earned = pw.IntegerField(default=None, null=True, constraints=[pw.Check('points_earned >= 0')])
    created_at = pw.DateTimeField(default=datetime.datetime.utcnow)
    updated_at = pw.DateTimeField(default=datetime.datetime.utcnow)

    def get_children(self) -> list[SubmissionNode]:
        """Get children nodes"""
        nodes = []
        for n in self.submissions_nodes:
            nodes.append(n)
        return nodes

    def can_see(self, user: User):
        """Indicate whether the user can see this submission"""
        if self.submitter == user:
            return True
        return self.is_complete and self.node.can_see_data(user)

    def validate(self) -> None:
        """
        Validate (and then submit) the Submission.
        Raise a ValidationError if the validation fail
        """
        from models.submission_node import SubmissionNode
        to_validate = self.node.get_rec_children_to_validate()
        validated_mandatory = list(SubmissionNode.select(SubmissionNode.id)
                                   .where((SubmissionNode.submission == self) & (SubmissionNode.node.in_(to_validate))))
        if len(validated_mandatory) != len(to_validate):
            nodes = []
            for n in to_validate:
                if n not in validated_mandatory:
                    nodes.append(n)
            raise NoData(nodes)
        validated = SubmissionNode.select().where(SubmissionNode.submission == self)
        for n in validated:
            n.validate_for_submission()
        self.is_complete = True

        points = CampaignNode.select(pw.fn.SUM(CampaignNode.points))\
            .join(SubmissionNode, pw.JOIN.LEFT_OUTER)\
            .where(SubmissionNode.submission == self)\
            .scalar()

        self.points_earned = points
        self.save()

        submitter = self.submitter
        submitter.points += points
        submitter.save()

    def flatten(self, nodes: list[CampaignNode]) -> dict[int, ]:
        """
        Transform values of the submission in a dict
        """
        from models.submission_node import SubmissionNode
        d = {
            n.node.id: SubmissionNode.flatten(n) for n in self.submissions_nodes if n.node in nodes
        }
        d.update(user_id=self.submitter)
        return d

    class Meta:
        database = db


def get_available_submissions(node: CampaignNode, user: User) -> List[Submission]:
    """Return the list of submission available to the user
    Either the user is the submitter, or the user have see_data permission
    """
    if node.can_see_data(user):
        return list(Submission.select().where(
            (Submission.node == node)
            & ((Submission.is_complete == True) | (Submission.submitter == user))))
    return list(Submission.select().where(
        (Submission.node == node)
        & (Submission.submitter == user)))


def get_completed_submissions(node: CampaignNode, user: User) -> List[Submission]:
    """Return the list of completed submission available to the user
    Either the user is the submitter, or the user have see_data permission
    """
    if node.can_see_data(user):
        return list(Submission.select().where(
            (Submission.node == node)
            & (Submission.is_complete == True)))
    return list(Submission.select().where(
        (Submission.node == node)
        & (Submission.is_complete == True)
        & (Submission.submitter == user)))


def get_submission_if_available(submission_id: int, user: User) -> Optional[Submission]:
    """Return the submission if it exists and if the user can see it
    """
    try:
        submission = Submission[submission_id]
    except Submission.DoesNotExist:
        return None
    if submission.can_see(user):
        return submission
    return None
