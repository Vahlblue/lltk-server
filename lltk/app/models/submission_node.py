#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
Definition of submission node model of a campaign and tools associated
"""
import datetime
import json
from typing import Optional, Dict

import peewee as pw
from playhouse.postgres_ext import BinaryJSONField
from pydantic import Json

from models.campaign_node import CampaignNode
from models.submission import Submission
from models.user import User
from tools.db import db


class SubmissionNode(pw.Model):
    """Submission object"""
    id = pw.AutoField()
    submission = pw.ForeignKeyField(Submission, backref="submissions_nodes", on_delete='CASCADE', on_update='CASCADE')
    node = pw.ForeignKeyField(CampaignNode, backref="submissions_nodes", on_delete='CASCADE', on_update='CASCADE')
    data = BinaryJSONField()
    created_at = pw.DateTimeField(default=datetime.datetime.utcnow)
    updated_at = pw.DateTimeField(default=datetime.datetime.utcnow)

    class Meta:
        database = db
        table_name = "submission_node"
        indexes = (
            (('submission', 'node'), True),
        )

    def can_see(self, user: User):
        """Indicate whether the user can see this submission node"""
        return self.submission.can_see(user)

    def validate_for_submission(self) -> None:
        """Validate the node if ready for submission
        Raise a ValidationError if the validation fail
        """
        # TODO
        pass

    def flatten(self) -> str:
        """
        Transform data of the submission in a str
        """
        match self.node.type:
            case "OPQ":
                return self.data["data"]["answer"]
            case "SLIDER":
                if self.node.data["is_double_slider"]:
                    return "%s;%s".format(self.data["data"]["first_value"], self.data["data"]["last_value"])
                else:
                    return self.data["data"]["first_value"]
            case "MCQ":
                print(json.loads(self.node.data["responses"]))
                return str([json.loads(self.node.data["responses"])[i]
                     for i, x in enumerate(self.data["data"]["chosen_answers"]) if x])
            case "DATE_PICKER":
                if self.node.data["single_date"]:
                    return "%s;%s".format(
                        datetime.datetime.fromtimestamp(self.data["data"]["first_date"] / 1e3),
                        datetime.datetime.fromtimestamp(self.data["data"]["final_date"] / 1e3))
                else:
                    return str(datetime.datetime.fromtimestamp(self.data["data"]["first_date"] / 1e3))
            case _:
                return json.dumps(self.data["data"])


def get_submission_node_if_available(submission_node_id: int, user: User) -> Optional[Submission]:
    """Return the submission if it exists and if the user can see it
    """
    try:
        submission_node = SubmissionNode[submission_node_id]
    except SubmissionNode.DoesNotExist:
        return None
    if submission_node.can_see(user):
        return submission_node
    return None


def get_submission_node_if_available_by_node(submission_id: int, node_id: int, user: User) -> Optional[Submission]:
    """Return the submission if it exists and if the user can see it
    """
    submission_node = SubmissionNode.get_or_none(submission=submission_id, node=node_id)
    if submission_node is None:
        return None
    if submission_node.can_see(user):
        return submission_node
    return None


def validate(node: CampaignNode, data_str: Json) -> None:
    """Validate the node data before insertion in database
    Raise a ValidationError if the validation fail
    """
    # TODO
    pass

